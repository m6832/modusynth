/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/components/NodeComponent.ts":
/*!*****************************************!*\
  !*** ./src/components/NodeComponent.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/v4.js");

/**
 * The graphical representation for a node. This does give accessors to
 * its coordinates (X and Y) and its dimensions (width and height).
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
var NodeComponent = /** @class */ (function () {
    function NodeComponent(wrapper) {
        this.wrapper = wrapper;
        this.uuid = (0,uuid__WEBPACK_IMPORTED_MODULE_0__["default"])();
        this.x = 20;
        this.y = 20;
        this.width = 200;
        this.height = 100;
    }
    return NodeComponent;
}());
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NodeComponent);


/***/ }),

/***/ "./src/config/tools.ts":
/*!*****************************!*\
  !*** ./src/config/tools.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _factories_createOscillator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../factories/createOscillator */ "./src/factories/createOscillator.ts");
/* harmony import */ var _wrappers_NumericParamWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../wrappers/NumericParamWrapper */ "./src/wrappers/NumericParamWrapper.ts");
/* harmony import */ var _utils_ToolNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/ToolNames */ "./src/utils/ToolNames.ts");



var tools = [
    {
        name: "inputs",
        tools: [
            {
                name: _utils_ToolNames__WEBPACK_IMPORTED_MODULE_2__["default"].OSCILLATOR,
                icon: "mdi-wave",
                factory: _factories_createOscillator__WEBPACK_IMPORTED_MODULE_0__["default"],
                parameters: [
                    {
                        name: "frequency",
                        minimum: 0,
                        maximum: 4000,
                        precision: .01,
                        wrapper: _wrappers_NumericParamWrapper__WEBPACK_IMPORTED_MODULE_1__["default"]
                    }
                ]
            }
        ]
    }
];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (tools);


/***/ }),

/***/ "./src/factories/createOscillator.ts":
/*!*******************************************!*\
  !*** ./src/factories/createOscillator.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ createOscillator)
/* harmony export */ });
var OSC_DEFAULT_FREQUENCY = 440;
var OSC_DEFAULT_DETUNE = 0;
var OSC_DEFAULT_TYPE = 'sine';
/**
 * Factory creating a oscillator with the accepted default values.
 *
 * @param {AudioContext} context The audio context in which we create the oscillator. The context
 *   MUST be shared between nodes to be able to connect them with one another.
 * @returns {OscillatorNode} An oscillator correctly parameterized.
 */
function createOscillator(context) {
    var oscillator = context.createOscillator();
    oscillator.type = OSC_DEFAULT_TYPE;
    oscillator.frequency.setValueAtTime(OSC_DEFAULT_FREQUENCY, context.currentTime);
    oscillator.detune.setValueAtTime(OSC_DEFAULT_DETUNE, context.currentTime);
    return oscillator;
}


/***/ }),

/***/ "./src/utils/ToolNames.ts":
/*!********************************!*\
  !*** ./src/utils/ToolNames.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var ToolNames;
(function (ToolNames) {
    ToolNames[ToolNames["OSCILLATOR"] = 0] = "OSCILLATOR";
})(ToolNames || (ToolNames = {}));
;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ToolNames);


/***/ }),

/***/ "./src/wrappers/NodeWrapper.ts":
/*!*************************************!*\
  !*** ./src/wrappers/NodeWrapper.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/v4.js");
/* harmony import */ var _components_NodeComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/NodeComponent */ "./src/components/NodeComponent.ts");


/**
 * A node wrapper contains an audio node as defined in the web audio API, for
 * exammple a GainNode for volume modifications purpose. THe wrapper can
 * dynamically build the parameters its corresponding Gui will have to display
 * by analyzing the configuration object given when building it.
 *
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
var NodeWrapper = /** @class */ (function () {
    function NodeWrapper(context, options) {
        this.uuid = (0,uuid__WEBPACK_IMPORTED_MODULE_1__["default"])();
        this.context = context;
        this.audioNode = options.factory(this.context);
        this.component = new _components_NodeComponent__WEBPACK_IMPORTED_MODULE_0__["default"](this);
    }
    return NodeWrapper;
}());
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NodeWrapper);


/***/ }),

/***/ "./src/wrappers/NumericParamWrapper.ts":
/*!*********************************************!*\
  !*** ./src/wrappers/NumericParamWrapper.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ParamWrapper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ParamWrapper */ "./src/wrappers/ParamWrapper.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * A numerical parameter is a parameter with a value being a number between
 * a maximum and a minimum, and its modifications being made with a given
 * precision. Most of the web audio API parameters fall whithin this category.
 *
 * @author Vincent Courtois <vincent.courtois@coddity.com>s
 */
var NumericParamWrapper = /** @class */ (function (_super) {
    __extends(NumericParamWrapper, _super);
    function NumericParamWrapper(node, options) {
        var _this = _super.call(this, node, options) || this;
        // The audio parameter object is dynamically set by retrieving it into
        // the web audio API node's parameters by the name. We can do it because
        // The name has not yet been edited by the user, even if the name is editable.
        var name = _this.name;
        _this.audioParam = node.audioNode[name];
        _this.minimum = options.minimum;
        _this.maximum = options.maximum;
        _this.precision = options.precision;
        return _this;
    }
    Object.defineProperty(NumericParamWrapper.prototype, "range", {
        /**
         * The difference between maximum and minimum, combined to the precision
         * this should help configuring the sliders displaying the parameters.
         */
        get: function () {
            return this.maximum - this.minimum;
        },
        enumerable: false,
        configurable: true
    });
    return NumericParamWrapper;
}(_ParamWrapper__WEBPACK_IMPORTED_MODULE_0__["default"]));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NumericParamWrapper);


/***/ }),

/***/ "./src/wrappers/ParamWrapper.ts":
/*!**************************************!*\
  !*** ./src/wrappers/ParamWrapper.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/v4.js");

/**
 * Holds the logic about the web audio API parameter it wraps, and the
 */
var ParamWrapper = /** @class */ (function () {
    function ParamWrapper(node, options) {
        this.uuid = (0,uuid__WEBPACK_IMPORTED_MODULE_0__["default"])();
        this.node = node;
        this.name = options.name;
    }
    return ParamWrapper;
}());
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ParamWrapper);


/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/regex.js":
/*!*****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/regex.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/rng.js":
/*!***************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/rng.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ rng)
/* harmony export */ });
// Unique ID creation requires a high quality random # generator. In the browser we therefore
// require the crypto API and do not support built-in fallback to lower quality random number
// generators (like Math.random()).
var getRandomValues;
var rnds8 = new Uint8Array(16);
function rng() {
  // lazy load so that environments that need to polyfill have a chance to do so
  if (!getRandomValues) {
    // getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
    // find the complete implementation of crypto (msCrypto) on IE11.
    getRandomValues = typeof crypto !== 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto !== 'undefined' && typeof msCrypto.getRandomValues === 'function' && msCrypto.getRandomValues.bind(msCrypto);

    if (!getRandomValues) {
      throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
    }
  }

  return getRandomValues(rnds8);
}

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/stringify.js":
/*!*********************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/stringify.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validate.js */ "./node_modules/uuid/dist/esm-browser/validate.js");

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */

var byteToHex = [];

for (var i = 0; i < 256; ++i) {
  byteToHex.push((i + 0x100).toString(16).substr(1));
}

function stringify(arr) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  var uuid = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + '-' + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + '-' + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + '-' + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + '-' + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase(); // Consistency check for valid UUID.  If this throws, it's likely due to one
  // of the following:
  // - One or more input array values don't map to a hex octet (leading to
  // "undefined" in the uuid)
  // - Invalid input values for the RFC `version` or `variant` fields

  if (!(0,_validate_js__WEBPACK_IMPORTED_MODULE_0__["default"])(uuid)) {
    throw TypeError('Stringified UUID is invalid');
  }

  return uuid;
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (stringify);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v4.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v4.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ "./node_modules/uuid/dist/esm-browser/rng.js");
/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stringify.js */ "./node_modules/uuid/dist/esm-browser/stringify.js");



function v4(options, buf, offset) {
  options = options || {};
  var rnds = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__["default"])(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`

  rnds[6] = rnds[6] & 0x0f | 0x40;
  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided

  if (buf) {
    offset = offset || 0;

    for (var i = 0; i < 16; ++i) {
      buf[offset + i] = rnds[i];
    }

    return buf;
  }

  return (0,_stringify_js__WEBPACK_IMPORTED_MODULE_1__["default"])(rnds);
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (v4);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/validate.js":
/*!********************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/validate.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _regex_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./regex.js */ "./node_modules/uuid/dist/esm-browser/regex.js");


function validate(uuid) {
  return typeof uuid === 'string' && _regex_js__WEBPACK_IMPORTED_MODULE_0__["default"].test(uuid);
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (validate);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "tools": () => (/* reexport safe */ _config_tools__WEBPACK_IMPORTED_MODULE_1__["default"]),
/* harmony export */   "NodeWrapper": () => (/* reexport safe */ _wrappers_NodeWrapper__WEBPACK_IMPORTED_MODULE_0__["default"]),
/* harmony export */   "ToolNames": () => (/* reexport safe */ _utils_ToolNames__WEBPACK_IMPORTED_MODULE_2__["default"])
/* harmony export */ });
/* harmony import */ var _wrappers_NodeWrapper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wrappers/NodeWrapper */ "./src/wrappers/NodeWrapper.ts");
/* harmony import */ var _config_tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config/tools */ "./src/config/tools.ts");
/* harmony import */ var _utils_ToolNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/ToolNames */ "./src/utils/ToolNames.ts");





})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFrQztBQUlsQzs7OztHQUlHO0FBQ0g7SUFhRSx1QkFBWSxPQUFvQjtRQUM5QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxHQUFHLGdEQUFJLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNaLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7SUFDcEIsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCNEQ7QUFDSztBQUN2QjtBQUUzQyxJQUFNLEtBQUssR0FBdUI7SUFDaEM7UUFDRSxJQUFJLEVBQUUsUUFBUTtRQUNkLEtBQUssRUFBRTtZQUNMO2dCQUNFLElBQUksRUFBRSxtRUFBb0I7Z0JBQzFCLElBQUksRUFBRSxVQUFVO2dCQUNoQixPQUFPLEVBQUUsbUVBQWdCO2dCQUN6QixVQUFVLEVBQUU7b0JBQ1Y7d0JBQ0UsSUFBSSxFQUFFLFdBQVc7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLE9BQU8sRUFBRSxJQUFJO3dCQUNiLFNBQVMsRUFBRSxHQUFHO3dCQUNkLE9BQU8sRUFBRSxxRUFBbUI7cUJBQzdCO2lCQUNGO2FBQ0Y7U0FDRjtLQUNGO0NBQ0YsQ0FBQztBQUVGLGlFQUFlLEtBQUssRUFBQzs7Ozs7Ozs7Ozs7Ozs7O0FDM0JyQixJQUFNLHFCQUFxQixHQUFHLEdBQUcsQ0FBQztBQUNsQyxJQUFNLGtCQUFrQixHQUFHLENBQUMsQ0FBQztBQUM3QixJQUFNLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztBQUVoQzs7Ozs7O0dBTUc7QUFDWSxTQUFTLGdCQUFnQixDQUFDLE9BQXFCO0lBQzVELElBQU0sVUFBVSxHQUFtQixPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM5RCxVQUFVLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO0lBQ25DLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNoRixVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDMUUsT0FBTyxVQUFVLENBQUM7QUFDcEIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDakJELElBQUssU0FFSjtBQUZELFdBQUssU0FBUztJQUNaLHFEQUFVO0FBQ1osQ0FBQyxFQUZJLFNBQVMsS0FBVCxTQUFTLFFBRWI7QUFBQSxDQUFDO0FBRUYsaUVBQWUsU0FBUyxFQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0hTO0FBRXNCO0FBR3hEOzs7Ozs7O0dBT0c7QUFDSDtJQVNFLHFCQUFZLE9BQXFCLEVBQUUsT0FBcUI7UUFDdEQsSUFBSSxDQUFDLElBQUksR0FBRyxnREFBSSxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksaUVBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFCd0M7QUFFekM7Ozs7OztHQU1HO0FBQ0g7SUFBaUQsdUNBQVk7SUFjM0QsNkJBQVksSUFBaUIsRUFBRSxPQUFtQztRQUFsRSxZQUNFLGtCQUFNLElBQUksRUFBRSxPQUFPLENBQUMsU0FXckI7UUFUQyxzRUFBc0U7UUFDdEUsd0VBQXdFO1FBQ3hFLDhFQUE4RTtRQUM5RSxJQUFNLElBQUksR0FBRyxLQUFJLENBQUMsSUFBdUIsQ0FBQztRQUMxQyxLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUEwQixDQUFDO1FBRWhFLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUMvQixLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7UUFDL0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDOztJQUNyQyxDQUFDO0lBTUQsc0JBQUksc0NBQUs7UUFKVDs7O1dBR0c7YUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3JDLENBQUM7OztPQUFBO0lBQ0gsMEJBQUM7QUFBRCxDQUFDLENBbkNnRCxxREFBWSxHQW1DNUQ7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUNpQztBQUlsQzs7R0FFRztBQUNIO0lBT0Usc0JBQVksSUFBaUIsRUFBRSxPQUEwQjtRQUN2RCxJQUFJLENBQUMsSUFBSSxHQUFHLGdEQUFJLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUNILG1CQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCRCxpRUFBZSxjQUFjLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEdBQUcseUNBQXlDOzs7Ozs7Ozs7Ozs7OztBQ0FwSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2U7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQ2xCcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsZ0JBQWdCLFNBQVM7QUFDekI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBnQkFBMGdCO0FBQzFnQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxPQUFPLHdEQUFRO0FBQ2Y7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlFQUFlLFNBQVM7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3Qkc7QUFDWTs7QUFFdkM7QUFDQTtBQUNBLCtDQUErQywrQ0FBRyxLQUFLOztBQUV2RDtBQUNBLG1DQUFtQzs7QUFFbkM7QUFDQTs7QUFFQSxvQkFBb0IsUUFBUTtBQUM1QjtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsU0FBUyx5REFBUztBQUNsQjs7QUFFQSxpRUFBZSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7QUN2QmM7O0FBRS9CO0FBQ0EscUNBQXFDLHNEQUFVO0FBQy9DOztBQUVBLGlFQUFlLFFBQVE7Ozs7OztVQ052QjtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7OztXQ3RCQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBOzs7OztXQ0FBO1dBQ0E7V0FDQTtXQUNBLHVEQUF1RCxpQkFBaUI7V0FDeEU7V0FDQSxnREFBZ0QsYUFBYTtXQUM3RDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0xnRDtBQUNkO0FBQ087QUFFUyIsInNvdXJjZXMiOlsid2VicGFjazovL0Btb2R1c3ludGgvY29yZS8uL3NyYy9jb21wb25lbnRzL05vZGVDb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlLy4vc3JjL2NvbmZpZy90b29scy50cyIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvLi9zcmMvZmFjdG9yaWVzL2NyZWF0ZU9zY2lsbGF0b3IudHMiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlLy4vc3JjL3V0aWxzL1Rvb2xOYW1lcy50cyIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvLi9zcmMvd3JhcHBlcnMvTm9kZVdyYXBwZXIudHMiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlLy4vc3JjL3dyYXBwZXJzL051bWVyaWNQYXJhbVdyYXBwZXIudHMiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlLy4vc3JjL3dyYXBwZXJzL1BhcmFtV3JhcHBlci50cyIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvLi9ub2RlX21vZHVsZXMvdXVpZC9kaXN0L2VzbS1icm93c2VyL3JlZ2V4LmpzIiwid2VicGFjazovL0Btb2R1c3ludGgvY29yZS8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvcm5nLmpzIiwid2VicGFjazovL0Btb2R1c3ludGgvY29yZS8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvc3RyaW5naWZ5LmpzIiwid2VicGFjazovL0Btb2R1c3ludGgvY29yZS8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvdjQuanMiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlLy4vbm9kZV9tb2R1bGVzL3V1aWQvZGlzdC9lc20tYnJvd3Nlci92YWxpZGF0ZS5qcyIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vQG1vZHVzeW50aC9jb3JlL3dlYnBhY2svcnVudGltZS9kZWZpbmUgcHJvcGVydHkgZ2V0dGVycyIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly9AbW9kdXN5bnRoL2NvcmUvLi9zcmMvaW5kZXgudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdjQgYXMgdXVpZCB9IGZyb20gJ3V1aWQnO1xuaW1wb3J0IElDb21wb25lbnQgZnJvbSBcIi4uL2ludGVyZmFjZXMvSUNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTm9kZVdyYXBwZXIgfSBmcm9tIFwiLi5cIlxuXG4vKipcbiAqIFRoZSBncmFwaGljYWwgcmVwcmVzZW50YXRpb24gZm9yIGEgbm9kZS4gVGhpcyBkb2VzIGdpdmUgYWNjZXNzb3JzIHRvXG4gKiBpdHMgY29vcmRpbmF0ZXMgKFggYW5kIFkpIGFuZCBpdHMgZGltZW5zaW9ucyAod2lkdGggYW5kIGhlaWdodCkuXG4gKiBAYXV0aG9yIFZpbmNlbnQgQ291cnRvaXMgPHZpbmNlbnQuY291cnRvaXNAY29kZGl0eS5jb20+XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE5vZGVDb21wb25lbnQgaW1wbGVtZW50cyBJQ29tcG9uZW50IHtcbiAgdXVpZDogc3RyaW5nO1xuICB3cmFwcGVyOiBOb2RlV3JhcHBlcjtcblxuICAvLyBUaGUgbnVtYmVyIG9mIHBpeGVscyBmcm9tIHRoZSBsZWZ0IHNpZGUgb2YgdGhlIHdpbmRvdy5cbiAgeDogbnVtYmVyO1xuICAvLyBUaGUgbnVtYmVyIG9mIHBpeGVscyBmcm9tIHRoZSB0b3Agb2YgdGhlIHdpbmRvdy5cbiAgeTogbnVtYmVyO1xuICAvLyBUaGUgbnVtYmVyIG9mIHBpeGVscyBiZXR3ZWVuIHRoZSBsZWZ0IGFuZCByaWdodCBzaWRlcyBvZiB0aGUgbm9kZS5cbiAgd2lkdGg6IG51bWJlcjtcbiAgLy8gVGhlIG51bWJlciBvZiBwaXhlbHMgYmV0d2VlbiB0aGUgdG9wIGFuZCBib3R0b20gb2YgdGhlIG5vZGUuXG4gIGhlaWdodDogbnVtYmVyO1xuICBcbiAgY29uc3RydWN0b3Iod3JhcHBlcjogTm9kZVdyYXBwZXIpIHtcbiAgICB0aGlzLndyYXBwZXIgPSB3cmFwcGVyO1xuICAgIHRoaXMudXVpZCA9IHV1aWQoKTtcbiAgICB0aGlzLnggPSAyMDtcbiAgICB0aGlzLnkgPSAyMDtcbiAgICB0aGlzLndpZHRoID0gMjAwO1xuICAgIHRoaXMuaGVpZ2h0ID0gMTAwO1xuICB9XG59IiwiaW1wb3J0IElDYXRlZ29yeU9wdGlvbnMgZnJvbSBcIi4uL2ludGVyZmFjZXMvb3B0aW9ucy9JQ2F0ZWdvcnlPcHRpb25zXCI7XG5pbXBvcnQgY3JlYXRlT3NjaWxsYXRvciBmcm9tIFwiLi4vZmFjdG9yaWVzL2NyZWF0ZU9zY2lsbGF0b3JcIjtcbmltcG9ydCBOdW1lcmljUGFyYW1XcmFwcGVyIGZyb20gXCIuLi93cmFwcGVycy9OdW1lcmljUGFyYW1XcmFwcGVyXCI7XG5pbXBvcnQgVG9vbE5hbWVzIGZyb20gXCIuLi91dGlscy9Ub29sTmFtZXNcIjtcblxuY29uc3QgdG9vbHM6IElDYXRlZ29yeU9wdGlvbnNbXSA9IFtcbiAge1xuICAgIG5hbWU6IFwiaW5wdXRzXCIsXG4gICAgdG9vbHM6IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogVG9vbE5hbWVzLk9TQ0lMTEFUT1IsXG4gICAgICAgIGljb246IFwibWRpLXdhdmVcIixcbiAgICAgICAgZmFjdG9yeTogY3JlYXRlT3NjaWxsYXRvcixcbiAgICAgICAgcGFyYW1ldGVyczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiZnJlcXVlbmN5XCIsXG4gICAgICAgICAgICBtaW5pbXVtOiAwLFxuICAgICAgICAgICAgbWF4aW11bTogNDAwMCxcbiAgICAgICAgICAgIHByZWNpc2lvbjogLjAxLFxuICAgICAgICAgICAgd3JhcHBlcjogTnVtZXJpY1BhcmFtV3JhcHBlclxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfVxuICAgIF1cbiAgfVxuXTtcblxuZXhwb3J0IGRlZmF1bHQgdG9vbHM7IiwiY29uc3QgT1NDX0RFRkFVTFRfRlJFUVVFTkNZID0gNDQwO1xuY29uc3QgT1NDX0RFRkFVTFRfREVUVU5FID0gMDtcbmNvbnN0IE9TQ19ERUZBVUxUX1RZUEUgPSAnc2luZSc7XG5cbi8qKlxuICogRmFjdG9yeSBjcmVhdGluZyBhIG9zY2lsbGF0b3Igd2l0aCB0aGUgYWNjZXB0ZWQgZGVmYXVsdCB2YWx1ZXMuXG4gKiBcbiAqIEBwYXJhbSB7QXVkaW9Db250ZXh0fSBjb250ZXh0IFRoZSBhdWRpbyBjb250ZXh0IGluIHdoaWNoIHdlIGNyZWF0ZSB0aGUgb3NjaWxsYXRvci4gVGhlIGNvbnRleHRcbiAqICAgTVVTVCBiZSBzaGFyZWQgYmV0d2VlbiBub2RlcyB0byBiZSBhYmxlIHRvIGNvbm5lY3QgdGhlbSB3aXRoIG9uZSBhbm90aGVyLlxuICogQHJldHVybnMge09zY2lsbGF0b3JOb2RlfSBBbiBvc2NpbGxhdG9yIGNvcnJlY3RseSBwYXJhbWV0ZXJpemVkLlxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBjcmVhdGVPc2NpbGxhdG9yKGNvbnRleHQ6IEF1ZGlvQ29udGV4dCk6IE9zY2lsbGF0b3JOb2RlIHtcbiAgY29uc3Qgb3NjaWxsYXRvcjogT3NjaWxsYXRvck5vZGUgPSBjb250ZXh0LmNyZWF0ZU9zY2lsbGF0b3IoKTtcbiAgb3NjaWxsYXRvci50eXBlID0gT1NDX0RFRkFVTFRfVFlQRTtcbiAgb3NjaWxsYXRvci5mcmVxdWVuY3kuc2V0VmFsdWVBdFRpbWUoT1NDX0RFRkFVTFRfRlJFUVVFTkNZLCBjb250ZXh0LmN1cnJlbnRUaW1lKTtcbiAgb3NjaWxsYXRvci5kZXR1bmUuc2V0VmFsdWVBdFRpbWUoT1NDX0RFRkFVTFRfREVUVU5FLCBjb250ZXh0LmN1cnJlbnRUaW1lKTtcbiAgcmV0dXJuIG9zY2lsbGF0b3I7XG59XG4iLCJlbnVtIFRvb2xOYW1lcyB7XG4gIE9TQ0lMTEFUT1Jcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRvb2xOYW1lczsiLCJpbXBvcnQgeyBJV3JhcHBlciB9IGZyb20gXCIuLlwiO1xuaW1wb3J0IHsgdjQgYXMgdXVpZCB9IGZyb20gJ3V1aWQnO1xuaW1wb3J0IElOb2RlT3B0aW9ucyBmcm9tIFwiLi4vaW50ZXJmYWNlcy9vcHRpb25zL0lOb2RlT3B0aW9uc1wiO1xuaW1wb3J0IE5vZGVDb21wb25lbnQgZnJvbSBcIi4uL2NvbXBvbmVudHMvTm9kZUNvbXBvbmVudFwiO1xuXG5cbi8qKlxuICogQSBub2RlIHdyYXBwZXIgY29udGFpbnMgYW4gYXVkaW8gbm9kZSBhcyBkZWZpbmVkIGluIHRoZSB3ZWIgYXVkaW8gQVBJLCBmb3JcbiAqIGV4YW1tcGxlIGEgR2Fpbk5vZGUgZm9yIHZvbHVtZSBtb2RpZmljYXRpb25zIHB1cnBvc2UuIFRIZSB3cmFwcGVyIGNhblxuICogZHluYW1pY2FsbHkgYnVpbGQgdGhlIHBhcmFtZXRlcnMgaXRzIGNvcnJlc3BvbmRpbmcgR3VpIHdpbGwgaGF2ZSB0byBkaXNwbGF5XG4gKiBieSBhbmFseXppbmcgdGhlIGNvbmZpZ3VyYXRpb24gb2JqZWN0IGdpdmVuIHdoZW4gYnVpbGRpbmcgaXQuXG4gKiBcbiAqIEBhdXRob3IgVmluY2VudCBDb3VydG9pcyA8dmluY2VudC5jb3VydG9pc0Bjb2RkaXR5LmNvbT5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTm9kZVdyYXBwZXIgaW1wbGVtZW50cyBJV3JhcHBlciB7XG4gIHV1aWQ6IHN0cmluZztcbiAgLy8gVGhlIFdlYiBBdWRpbyBBUEkgbm9kZSB3cmFwcGVkIGJ5IHRoaXMgb2JqZWN0LlxuICBhdWRpb05vZGU6IEF1ZGlvTm9kZTtcbiAgLy8gVGhlIGF1ZGlvIGNvbnRleHQsIG1haW5seSB1c2VkIHRvIGdldCB0aGUgY29udGV4dCdzIGN1cnJlbnQgdGltZS5cbiAgY29udGV4dDogQXVkaW9Db250ZXh0O1xuXG4gIGNvbXBvbmVudDogTm9kZUNvbXBvbmVudDtcblxuICBjb25zdHJ1Y3Rvcihjb250ZXh0OiBBdWRpb0NvbnRleHQsIG9wdGlvbnM6IElOb2RlT3B0aW9ucykge1xuICAgIHRoaXMudXVpZCA9IHV1aWQoKTtcbiAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgIHRoaXMuYXVkaW9Ob2RlID0gb3B0aW9ucy5mYWN0b3J5KHRoaXMuY29udGV4dCk7XG4gICAgdGhpcy5jb21wb25lbnQgPSBuZXcgTm9kZUNvbXBvbmVudCh0aGlzKTtcbiAgfVxufSIsImltcG9ydCB7IE5vZGVXcmFwcGVyIH0gZnJvbSBcIi4uXCI7XG5pbXBvcnQgSU51bWVyaWNhbFBhcmFtZXRlck9wdGlvbnMgZnJvbSBcIi4uL2ludGVyZmFjZXMvb3B0aW9ucy9JTnVtZXJpY2FsUGFyYW1ldGVyT3B0aW9uc1wiO1xuaW1wb3J0IElQYXJhbWV0ZXJPcHRpb25zIGZyb20gXCIuLi9pbnRlcmZhY2VzL29wdGlvbnMvSVBhcmFtZXRlck9wdGlvbnNcIjtcbmltcG9ydCBQYXJhbVdyYXBwZXIgZnJvbSBcIi4vUGFyYW1XcmFwcGVyXCJcblxuLyoqXG4gKiBBIG51bWVyaWNhbCBwYXJhbWV0ZXIgaXMgYSBwYXJhbWV0ZXIgd2l0aCBhIHZhbHVlIGJlaW5nIGEgbnVtYmVyIGJldHdlZW5cbiAqIGEgbWF4aW11bSBhbmQgYSBtaW5pbXVtLCBhbmQgaXRzIG1vZGlmaWNhdGlvbnMgYmVpbmcgbWFkZSB3aXRoIGEgZ2l2ZW5cbiAqIHByZWNpc2lvbi4gTW9zdCBvZiB0aGUgd2ViIGF1ZGlvIEFQSSBwYXJhbWV0ZXJzIGZhbGwgd2hpdGhpbiB0aGlzIGNhdGVnb3J5LlxuICogXG4gKiBAYXV0aG9yIFZpbmNlbnQgQ291cnRvaXMgPHZpbmNlbnQuY291cnRvaXNAY29kZGl0eS5jb20+c1xuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOdW1lcmljUGFyYW1XcmFwcGVyIGV4dGVuZHMgUGFyYW1XcmFwcGVyIHtcblxuICAvKipcbiAgICogVGhlIGF1ZGlvIHBhcmFtZXRlciB3cmFwcGVkIGJ5IHRoaXMgb2JqZWN0LiBBcyBhbiBBLXBhcmFtIGl0IHNob3VsZCBiZVxuICAgKiBhY2Nlc3NlZCBhbmQgc2V0IHZpYSB0aGUgLnZhbHVlIGF0dHJpYnV0ZSBhbmQgbm90IGRpcmVjdGx5LlxuICAgKi9cbiAgYXVkaW9QYXJhbTogQXVkaW9QYXJhbTtcbiAgLy8gVGhlIHNtYWxsZXN0IHZhbHVlIHRoZSBwYXJhbWV0ZXIgc2hvdWxkIGJlIGFsbG93ZWQgdG8gYmUgc2V0IGF0LlxuICBtaW5pbXVtOiBudW1iZXI7XG4gIC8vIFRoZSBncmVhdGVzdCB2YWx1ZSB0aGUgcGFyYW1ldGVyIHNob3VsZCBiZSBhbGxvd2VkIHRvIGJlIHNldCBhdC5cbiAgbWF4aW11bTogbnVtYmVyO1xuICAvLyBUaGUgbnVtZXJpY2FsIHN0ZXAgYmV0d2VlbiB0d28gdmFsdWVzIG9mIHRoZSBwYXJhbWV0ZXIuXG4gIHByZWNpc2lvbjogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKG5vZGU6IE5vZGVXcmFwcGVyLCBvcHRpb25zOiBJTnVtZXJpY2FsUGFyYW1ldGVyT3B0aW9ucykge1xuICAgIHN1cGVyKG5vZGUsIG9wdGlvbnMpO1xuXG4gICAgLy8gVGhlIGF1ZGlvIHBhcmFtZXRlciBvYmplY3QgaXMgZHluYW1pY2FsbHkgc2V0IGJ5IHJldHJpZXZpbmcgaXQgaW50b1xuICAgIC8vIHRoZSB3ZWIgYXVkaW8gQVBJIG5vZGUncyBwYXJhbWV0ZXJzIGJ5IHRoZSBuYW1lLiBXZSBjYW4gZG8gaXQgYmVjYXVzZVxuICAgIC8vIFRoZSBuYW1lIGhhcyBub3QgeWV0IGJlZW4gZWRpdGVkIGJ5IHRoZSB1c2VyLCBldmVuIGlmIHRoZSBuYW1lIGlzIGVkaXRhYmxlLlxuICAgIGNvbnN0IG5hbWUgPSB0aGlzLm5hbWUgYXMga2V5b2YgQXVkaW9Ob2RlO1xuICAgIHRoaXMuYXVkaW9QYXJhbSA9IG5vZGUuYXVkaW9Ob2RlW25hbWVdIGFzIHVua25vd24gYXMgQXVkaW9QYXJhbTtcblxuICAgIHRoaXMubWluaW11bSA9IG9wdGlvbnMubWluaW11bTtcbiAgICB0aGlzLm1heGltdW0gPSBvcHRpb25zLm1heGltdW07XG4gICAgdGhpcy5wcmVjaXNpb24gPSBvcHRpb25zLnByZWNpc2lvbjtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGUgZGlmZmVyZW5jZSBiZXR3ZWVuIG1heGltdW0gYW5kIG1pbmltdW0sIGNvbWJpbmVkIHRvIHRoZSBwcmVjaXNpb25cbiAgICogdGhpcyBzaG91bGQgaGVscCBjb25maWd1cmluZyB0aGUgc2xpZGVycyBkaXNwbGF5aW5nIHRoZSBwYXJhbWV0ZXJzLlxuICAgKi9cbiAgZ2V0IHJhbmdlKCkge1xuICAgIHJldHVybiB0aGlzLm1heGltdW0gLSB0aGlzLm1pbmltdW07XG4gIH1cbn0iLCJpbXBvcnQgSVdyYXBwZXIgZnJvbSBcIi4uL2ludGVyZmFjZXMvSVdyYXBwZXJcIjtcbmltcG9ydCB7IHY0IGFzIHV1aWQgfSBmcm9tIFwidXVpZFwiO1xuaW1wb3J0IElQYXJhbWV0ZXJPcHRpb25zIGZyb20gXCIuLi9pbnRlcmZhY2VzL29wdGlvbnMvSVBhcmFtZXRlck9wdGlvbnNcIjtcbmltcG9ydCB7IE5vZGVXcmFwcGVyIH0gZnJvbSBcIi4uXCI7XG5cbi8qKlxuICogSG9sZHMgdGhlIGxvZ2ljIGFib3V0IHRoZSB3ZWIgYXVkaW8gQVBJIHBhcmFtZXRlciBpdCB3cmFwcywgYW5kIHRoZSBcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGFyYW1XcmFwcGVyIGltcGxlbWVudHMgSVdyYXBwZXIge1xuICB1dWlkOiBzdHJpbmc7XG4gIC8vIFRoZSBub2RlIGNvbnRhaW5pbmcgdGhpcyBwYXJhbWV0ZXIuXG4gIG5vZGU6IE5vZGVXcmFwcGVyO1xuICAvLyBUaGUgbmFtZSBvZiB0aGUgcGFyYW1ldGVyLiBUaGlzIENPVUxEIGJlIG1hZGUgZWRpdGFibGUsIGJ1dCBpdCBpcyBub3QgbWFuZGF0b3J5LlxuICBuYW1lOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iobm9kZTogTm9kZVdyYXBwZXIsIG9wdGlvbnM6IElQYXJhbWV0ZXJPcHRpb25zKSB7XG4gICAgdGhpcy51dWlkID0gdXVpZCgpO1xuICAgIHRoaXMubm9kZSA9IG5vZGU7XG4gICAgdGhpcy5uYW1lID0gb3B0aW9ucy5uYW1lO1xuICB9XG59IiwiZXhwb3J0IGRlZmF1bHQgL14oPzpbMC05YS1mXXs4fS1bMC05YS1mXXs0fS1bMS01XVswLTlhLWZdezN9LVs4OWFiXVswLTlhLWZdezN9LVswLTlhLWZdezEyfXwwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDApJC9pOyIsIi8vIFVuaXF1ZSBJRCBjcmVhdGlvbiByZXF1aXJlcyBhIGhpZ2ggcXVhbGl0eSByYW5kb20gIyBnZW5lcmF0b3IuIEluIHRoZSBicm93c2VyIHdlIHRoZXJlZm9yZVxuLy8gcmVxdWlyZSB0aGUgY3J5cHRvIEFQSSBhbmQgZG8gbm90IHN1cHBvcnQgYnVpbHQtaW4gZmFsbGJhY2sgdG8gbG93ZXIgcXVhbGl0eSByYW5kb20gbnVtYmVyXG4vLyBnZW5lcmF0b3JzIChsaWtlIE1hdGgucmFuZG9tKCkpLlxudmFyIGdldFJhbmRvbVZhbHVlcztcbnZhciBybmRzOCA9IG5ldyBVaW50OEFycmF5KDE2KTtcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHJuZygpIHtcbiAgLy8gbGF6eSBsb2FkIHNvIHRoYXQgZW52aXJvbm1lbnRzIHRoYXQgbmVlZCB0byBwb2x5ZmlsbCBoYXZlIGEgY2hhbmNlIHRvIGRvIHNvXG4gIGlmICghZ2V0UmFuZG9tVmFsdWVzKSB7XG4gICAgLy8gZ2V0UmFuZG9tVmFsdWVzIG5lZWRzIHRvIGJlIGludm9rZWQgaW4gYSBjb250ZXh0IHdoZXJlIFwidGhpc1wiIGlzIGEgQ3J5cHRvIGltcGxlbWVudGF0aW9uLiBBbHNvLFxuICAgIC8vIGZpbmQgdGhlIGNvbXBsZXRlIGltcGxlbWVudGF0aW9uIG9mIGNyeXB0byAobXNDcnlwdG8pIG9uIElFMTEuXG4gICAgZ2V0UmFuZG9tVmFsdWVzID0gdHlwZW9mIGNyeXB0byAhPT0gJ3VuZGVmaW5lZCcgJiYgY3J5cHRvLmdldFJhbmRvbVZhbHVlcyAmJiBjcnlwdG8uZ2V0UmFuZG9tVmFsdWVzLmJpbmQoY3J5cHRvKSB8fCB0eXBlb2YgbXNDcnlwdG8gIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBtc0NyeXB0by5nZXRSYW5kb21WYWx1ZXMgPT09ICdmdW5jdGlvbicgJiYgbXNDcnlwdG8uZ2V0UmFuZG9tVmFsdWVzLmJpbmQobXNDcnlwdG8pO1xuXG4gICAgaWYgKCFnZXRSYW5kb21WYWx1ZXMpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignY3J5cHRvLmdldFJhbmRvbVZhbHVlcygpIG5vdCBzdXBwb3J0ZWQuIFNlZSBodHRwczovL2dpdGh1Yi5jb20vdXVpZGpzL3V1aWQjZ2V0cmFuZG9tdmFsdWVzLW5vdC1zdXBwb3J0ZWQnKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZ2V0UmFuZG9tVmFsdWVzKHJuZHM4KTtcbn0iLCJpbXBvcnQgdmFsaWRhdGUgZnJvbSAnLi92YWxpZGF0ZS5qcyc7XG4vKipcbiAqIENvbnZlcnQgYXJyYXkgb2YgMTYgYnl0ZSB2YWx1ZXMgdG8gVVVJRCBzdHJpbmcgZm9ybWF0IG9mIHRoZSBmb3JtOlxuICogWFhYWFhYWFgtWFhYWC1YWFhYLVhYWFgtWFhYWFhYWFhYWFhYXG4gKi9cblxudmFyIGJ5dGVUb0hleCA9IFtdO1xuXG5mb3IgKHZhciBpID0gMDsgaSA8IDI1NjsgKytpKSB7XG4gIGJ5dGVUb0hleC5wdXNoKChpICsgMHgxMDApLnRvU3RyaW5nKDE2KS5zdWJzdHIoMSkpO1xufVxuXG5mdW5jdGlvbiBzdHJpbmdpZnkoYXJyKSB7XG4gIHZhciBvZmZzZXQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IDA7XG4gIC8vIE5vdGU6IEJlIGNhcmVmdWwgZWRpdGluZyB0aGlzIGNvZGUhICBJdCdzIGJlZW4gdHVuZWQgZm9yIHBlcmZvcm1hbmNlXG4gIC8vIGFuZCB3b3JrcyBpbiB3YXlzIHlvdSBtYXkgbm90IGV4cGVjdC4gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS91dWlkanMvdXVpZC9wdWxsLzQzNFxuICB2YXIgdXVpZCA9IChieXRlVG9IZXhbYXJyW29mZnNldCArIDBdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMV1dICsgYnl0ZVRvSGV4W2FycltvZmZzZXQgKyAyXV0gKyBieXRlVG9IZXhbYXJyW29mZnNldCArIDNdXSArICctJyArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgNF1dICsgYnl0ZVRvSGV4W2FycltvZmZzZXQgKyA1XV0gKyAnLScgKyBieXRlVG9IZXhbYXJyW29mZnNldCArIDZdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgN11dICsgJy0nICsgYnl0ZVRvSGV4W2FycltvZmZzZXQgKyA4XV0gKyBieXRlVG9IZXhbYXJyW29mZnNldCArIDldXSArICctJyArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTBdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTFdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTJdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTNdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTRdXSArIGJ5dGVUb0hleFthcnJbb2Zmc2V0ICsgMTVdXSkudG9Mb3dlckNhc2UoKTsgLy8gQ29uc2lzdGVuY3kgY2hlY2sgZm9yIHZhbGlkIFVVSUQuICBJZiB0aGlzIHRocm93cywgaXQncyBsaWtlbHkgZHVlIHRvIG9uZVxuICAvLyBvZiB0aGUgZm9sbG93aW5nOlxuICAvLyAtIE9uZSBvciBtb3JlIGlucHV0IGFycmF5IHZhbHVlcyBkb24ndCBtYXAgdG8gYSBoZXggb2N0ZXQgKGxlYWRpbmcgdG9cbiAgLy8gXCJ1bmRlZmluZWRcIiBpbiB0aGUgdXVpZClcbiAgLy8gLSBJbnZhbGlkIGlucHV0IHZhbHVlcyBmb3IgdGhlIFJGQyBgdmVyc2lvbmAgb3IgYHZhcmlhbnRgIGZpZWxkc1xuXG4gIGlmICghdmFsaWRhdGUodXVpZCkpIHtcbiAgICB0aHJvdyBUeXBlRXJyb3IoJ1N0cmluZ2lmaWVkIFVVSUQgaXMgaW52YWxpZCcpO1xuICB9XG5cbiAgcmV0dXJuIHV1aWQ7XG59XG5cbmV4cG9ydCBkZWZhdWx0IHN0cmluZ2lmeTsiLCJpbXBvcnQgcm5nIGZyb20gJy4vcm5nLmpzJztcbmltcG9ydCBzdHJpbmdpZnkgZnJvbSAnLi9zdHJpbmdpZnkuanMnO1xuXG5mdW5jdGlvbiB2NChvcHRpb25zLCBidWYsIG9mZnNldCkge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHJuZHMgPSBvcHRpb25zLnJhbmRvbSB8fCAob3B0aW9ucy5ybmcgfHwgcm5nKSgpOyAvLyBQZXIgNC40LCBzZXQgYml0cyBmb3IgdmVyc2lvbiBhbmQgYGNsb2NrX3NlcV9oaV9hbmRfcmVzZXJ2ZWRgXG5cbiAgcm5kc1s2XSA9IHJuZHNbNl0gJiAweDBmIHwgMHg0MDtcbiAgcm5kc1s4XSA9IHJuZHNbOF0gJiAweDNmIHwgMHg4MDsgLy8gQ29weSBieXRlcyB0byBidWZmZXIsIGlmIHByb3ZpZGVkXG5cbiAgaWYgKGJ1Zikge1xuICAgIG9mZnNldCA9IG9mZnNldCB8fCAwO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCAxNjsgKytpKSB7XG4gICAgICBidWZbb2Zmc2V0ICsgaV0gPSBybmRzW2ldO1xuICAgIH1cblxuICAgIHJldHVybiBidWY7XG4gIH1cblxuICByZXR1cm4gc3RyaW5naWZ5KHJuZHMpO1xufVxuXG5leHBvcnQgZGVmYXVsdCB2NDsiLCJpbXBvcnQgUkVHRVggZnJvbSAnLi9yZWdleC5qcyc7XG5cbmZ1bmN0aW9uIHZhbGlkYXRlKHV1aWQpIHtcbiAgcmV0dXJuIHR5cGVvZiB1dWlkID09PSAnc3RyaW5nJyAmJiBSRUdFWC50ZXN0KHV1aWQpO1xufVxuXG5leHBvcnQgZGVmYXVsdCB2YWxpZGF0ZTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLy8gZGVmaW5lIGdldHRlciBmdW5jdGlvbnMgZm9yIGhhcm1vbnkgZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5kID0gKGV4cG9ydHMsIGRlZmluaXRpb24pID0+IHtcblx0Zm9yKHZhciBrZXkgaW4gZGVmaW5pdGlvbikge1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhkZWZpbml0aW9uLCBrZXkpICYmICFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywga2V5KSkge1xuXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIGtleSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGRlZmluaXRpb25ba2V5XSB9KTtcblx0XHR9XG5cdH1cbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5vID0gKG9iaiwgcHJvcCkgPT4gKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIHByb3ApKSIsIi8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uciA9IChleHBvcnRzKSA9PiB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsImltcG9ydCBJV3JhcHBlciBmcm9tICcuL2ludGVyZmFjZXMvSVdyYXBwZXInXG5pbXBvcnQgTm9kZVdyYXBwZXIgZnJvbSAnLi93cmFwcGVycy9Ob2RlV3JhcHBlcidcbmltcG9ydCB0b29scyBmcm9tICcuL2NvbmZpZy90b29scydcbmltcG9ydCBUb29sTmFtZXMgZnJvbSAnLi91dGlscy9Ub29sTmFtZXMnXG5cbmV4cG9ydCB7IHRvb2xzLCBJV3JhcHBlciwgTm9kZVdyYXBwZXIsIFRvb2xOYW1lcyB9Il0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9