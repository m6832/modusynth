const OSC_DEFAULT_FREQUENCY = 440;
const OSC_DEFAULT_DETUNE = 0;
const OSC_DEFAULT_TYPE = 'sine';

/**
 * Factory creating a oscillator with the accepted default values.
 * 
 * @param {AudioContext} context The audio context in which we create the oscillator. The context
 *   MUST be shared between nodes to be able to connect them with one another.
 * @returns {OscillatorNode} An oscillator correctly parameterized.
 */
export default function createOscillator(context: AudioContext): OscillatorNode {
  const oscillator: OscillatorNode = context.createOscillator();
  oscillator.type = OSC_DEFAULT_TYPE;
  oscillator.frequency.setValueAtTime(OSC_DEFAULT_FREQUENCY, context.currentTime);
  oscillator.detune.setValueAtTime(OSC_DEFAULT_DETUNE, context.currentTime);
  return oscillator;
}
