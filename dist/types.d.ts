declare module "webpack.config" {
    export const entry: string;
    export namespace output {
        const path: string;
        const filename: string;
    }
    export const mode: string;
    export namespace module {
        const rules: {
            test: RegExp;
            exclude: RegExp;
            loader: string;
        }[];
    }
    export namespace resolve {
        const extensions: string[];
    }
    export const devtool: string;
}
declare module "src/interfaces/IComponent" {
    import { IWrapper } from "src/index";
    /**
     * A component represents the logical object wrapped in the corresponding
     * wrapper in the GUI. It usually provides methods and attributes to access
     * things like coordinates, width or height.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default interface IComponent {
        uuid: string;
        wrapper: IWrapper;
    }
}
declare module "src/interfaces/IWrapper" {
    /**
     * A wrapper is a logical container for an audio element, whether it is
     * a node, a link, a port or a parameter. The main wrapper interface holds
     * the logic for all the generic behaviours :
     * - Identifying an element with its unique UUID.
     * - linking the wrapper to a GUI component inheriting from IGui.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default interface IWrapper {
        /**
         * The unique identifier of the Wrapper, useful to track down some bugs
         * by knowing if two elements are referenced as the same component or not.
         */
        uuid: string;
    }
}
declare module "src/wrappers/ParamWrapper" {
    import IWrapper from "src/interfaces/IWrapper";
    import IParameterOptions from "src/interfaces/options/IParameterOptions";
    import { NodeWrapper } from "src/index";
    /**
     * Holds the logic about the web audio API parameter it wraps, and the
     */
    export default class ParamWrapper implements IWrapper {
        uuid: string;
        node: NodeWrapper;
        name: string;
        constructor(node: NodeWrapper, options: IParameterOptions);
    }
}
declare module "src/interfaces/options/IParameterOptions" {
    import ParamWrapper from "src/wrappers/ParamWrapper";
    /**
     * This interface holds fields describing a parameter linked to a
     * node creation tool in the tools bar. It indicates which component
     * should be displaying this parameter in the UI.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default interface IParameterOptions {
        /**
         * The name of the parameter, giving the instancied ParamWrapper
         * its default name (editable by the user).
         */
        name: string;
        /**
         * Holds the class of the wrapper that will be instanciated when
         * creating the wrapper for this parameter configuration.
         */
        wrapper: typeof ParamWrapper;
    }
}
declare module "src/interfaces/options/INumericalParameterOptions" {
    import IParameterOptions from "src/interfaces/options/IParameterOptions";
    export default interface INumericalParameterOptions extends IParameterOptions {
        minimum: number;
        maximum: number;
        precision: number;
    }
}
declare module "src/interfaces/options/INodeOptions" {
    import { ToolNames } from "src/index";
    import INumericalParameterOptions from "src/interfaces/options/INumericalParameterOptions";
    type ParamConfig = INumericalParameterOptions;
    /**
     * The configuration given to a node when building it. A configuration
     * object passed to a wrapper is then passed to its corresponding Gui.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default interface INodeOptions {
        /**
         * The default name of the created node. The name of a node is made
         * to be editable, but it has to have a default value.
         */
        name: ToolNames;
        /**
         * The icon to represent the tool in the toolbox. This is supposed
         * to be a material design icon, but it should be possible to use
         * font-awesome icons as well.
         */
        icon: string;
        /**
         * the factoery function used to create the node of the web audio API
         * wrapped in the created IWrapper sub-class instance.
         */
        factory: Function;
        /**
         * The parameters that will be instanciated as ParamWrapper instances
         * linked to the main NodeWrapper instance.
         */
        parameters: ParamConfig[];
    }
}
declare module "src/components/NodeComponent" {
    import IComponent from "src/interfaces/IComponent";
    import { NodeWrapper } from "src/index";
    /**
     * The graphical representation for a node. This does give accessors to
     * its coordinates (X and Y) and its dimensions (width and height).
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default class NodeComponent implements IComponent {
        uuid: string;
        wrapper: NodeWrapper;
        x: number;
        y: number;
        width: number;
        height: number;
        constructor(wrapper: NodeWrapper);
    }
}
declare module "src/wrappers/NodeWrapper" {
    import { IWrapper } from "src/index";
    import INodeOptions from "src/interfaces/options/INodeOptions";
    import NodeComponent from "src/components/NodeComponent";
    /**
     * A node wrapper contains an audio node as defined in the web audio API, for
     * exammple a GainNode for volume modifications purpose. THe wrapper can
     * dynamically build the parameters its corresponding Gui will have to display
     * by analyzing the configuration object given when building it.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default class NodeWrapper implements IWrapper {
        uuid: string;
        audioNode: AudioNode;
        context: AudioContext;
        component: NodeComponent;
        constructor(context: AudioContext, options: INodeOptions);
    }
}
declare module "src/interfaces/options/ICategoryOptions" {
    import INodeOptions from "src/interfaces/options/INodeOptions";
    /**
     * Represents a category of tools in the toolbox.
     * @author Vincent Courtois <vincent.courtois@coddity.com>
     */
    export default interface ICategoryOptions {
        name: string;
        tools: INodeOptions[];
    }
}
declare module "src/factories/createOscillator" {
    /**
     * Factory creating a oscillator with the accepted default values.
     *
     * @param {AudioContext} context The audio context in which we create the oscillator. The context
     *   MUST be shared between nodes to be able to connect them with one another.
     * @returns {OscillatorNode} An oscillator correctly parameterized.
     */
    export default function createOscillator(context: AudioContext): OscillatorNode;
}
declare module "src/wrappers/NumericParamWrapper" {
    import { NodeWrapper } from "src/index";
    import INumericalParameterOptions from "src/interfaces/options/INumericalParameterOptions";
    import ParamWrapper from "src/wrappers/ParamWrapper";
    /**
     * A numerical parameter is a parameter with a value being a number between
     * a maximum and a minimum, and its modifications being made with a given
     * precision. Most of the web audio API parameters fall whithin this category.
     *
     * @author Vincent Courtois <vincent.courtois@coddity.com>s
     */
    export default class NumericParamWrapper extends ParamWrapper {
        /**
         * The audio parameter wrapped by this object. As an A-param it should be
         * accessed and set via the .value attribute and not directly.
         */
        audioParam: AudioParam;
        minimum: number;
        maximum: number;
        precision: number;
        constructor(node: NodeWrapper, options: INumericalParameterOptions);
        /**
         * The difference between maximum and minimum, combined to the precision
         * this should help configuring the sliders displaying the parameters.
         */
        get range(): number;
    }
}
declare module "src/utils/ToolNames" {
    enum ToolNames {
        OSCILLATOR = 0
    }
    export default ToolNames;
}
declare module "src/config/tools" {
    import ICategoryOptions from "src/interfaces/options/ICategoryOptions";
    const tools: ICategoryOptions[];
    export default tools;
}
declare module "src/index" {
    import IWrapper from "src/interfaces/IWrapper";
    import NodeWrapper from "src/wrappers/NodeWrapper";
    import tools from "src/config/tools";
    import ToolNames from "src/utils/ToolNames";
    export { tools, IWrapper, NodeWrapper, ToolNames };
}
declare module "src/utils/NodesList" {
    import { NodeWrapper } from "src/index";
    export default class NodesList {
        list: NodeWrapper[];
        constructor();
        /**
         * Determines if a node is in the list by searching for its UUID.
         * @param {NodeWrapper} node The node to check the existence in the list.
         * @return {boolean} TRUE if the node is in the list, FALSE otherwise.
         */
        contains(node: NodeWrapper): boolean;
        /**
         * Adds a node to the current list if it does not exist already.
         * @param node the node to add to the current list.
         */
        add(node: NodeWrapper): void;
        indexOf(node: NodeWrapper): number;
        remove(node: NodeWrapper): void;
    }
}
declare module "src/vuex/modules/nodes/state" {
    import NodesList from "src/utils/NodesList";
    export interface NodesState {
        nodes: NodesList;
    }
    export const state: NodesState;
}
declare module "src/vuex/modules/nodes/mutations" {
    import { MutationTree } from 'vuex';
    import { NodesState } from "src/vuex/modules/nodes/state";
    export const mutations: MutationTree<NodesState>;
}
//# sourceMappingURL=types.d.ts.map