import { IWrapper } from "..";

/**
 * A component represents the logical object wrapped in the corresponding
 * wrapper in the GUI. It usually provides methods and attributes to access
 * things like coordinates, width or height.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default interface IComponent {
  // The unique UUID for this component.
  uuid: string;
  // The logical object represented by this component.
  wrapper: IWrapper;
}