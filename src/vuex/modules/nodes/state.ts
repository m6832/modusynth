import NodesList from "../../../utils/NodesList";

export interface NodesState {
  nodes: NodesList;
};

export const state: NodesState = {
  nodes: new NodesList(),
};