import { MutationTree } from 'vuex';
import { NodeWrapper } from '../../..';
import { NodesState } from './state';

export const mutations: MutationTree<NodesState> = {
  addNode(state: NodesState, node: NodeWrapper) {
    state.nodes.add(node);
  },
  removeNode(state: NodesState, node: NodeWrapper) {
    state.nodes.remove(node);
  }
}