import { NodeWrapper } from "..";
import INumericalParameterOptions from "../interfaces/options/INumericalParameterOptions";
import IParameterOptions from "../interfaces/options/IParameterOptions";
import ParamWrapper from "./ParamWrapper"

/**
 * A numerical parameter is a parameter with a value being a number between
 * a maximum and a minimum, and its modifications being made with a given
 * precision. Most of the web audio API parameters fall whithin this category.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>s
 */
export default class NumericParamWrapper extends ParamWrapper {

  /**
   * The audio parameter wrapped by this object. As an A-param it should be
   * accessed and set via the .value attribute and not directly.
   */
  audioParam: AudioParam;
  // The smallest value the parameter should be allowed to be set at.
  minimum: number;
  // The greatest value the parameter should be allowed to be set at.
  maximum: number;
  // The numerical step between two values of the parameter.
  precision: number;

  constructor(node: NodeWrapper, options: INumericalParameterOptions) {
    super(node, options);

    // The audio parameter object is dynamically set by retrieving it into
    // the web audio API node's parameters by the name. We can do it because
    // The name has not yet been edited by the user, even if the name is editable.
    const name = this.name as keyof AudioNode;
    this.audioParam = node.audioNode[name] as unknown as AudioParam;

    this.minimum = options.minimum;
    this.maximum = options.maximum;
    this.precision = options.precision;
  }

  /**
   * The difference between maximum and minimum, combined to the precision
   * this should help configuring the sliders displaying the parameters.
   */
  get range() {
    return this.maximum - this.minimum;
  }
}