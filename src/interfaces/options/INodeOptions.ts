import { ToolNames } from "../..";
import INumericalParameterOptions from "./INumericalParameterOptions";
import IParameterOptions from "./IParameterOptions";

type ParamConfig = INumericalParameterOptions;

/**
 * The configuration given to a node when building it. A configuration
 * object passed to a wrapper is then passed to its corresponding Gui.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default interface INodeOptions {
  /**
   * The default name of the created node. The name of a node is made
   * to be editable, but it has to have a default value.
   */
  name: ToolNames;

  /**
   * The icon to represent the tool in the toolbox. This is supposed
   * to be a material design icon, but it should be possible to use
   * font-awesome icons as well.
   */
  icon: string;

  /**
   * the factoery function used to create the node of the web audio API
   * wrapped in the created IWrapper sub-class instance.
   */
  factory: Function;

  /**
   * The parameters that will be instanciated as ParamWrapper instances
   * linked to the main NodeWrapper instance.
   */
  parameters: ParamConfig[]
}