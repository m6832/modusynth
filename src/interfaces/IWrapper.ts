import IComponent from "./IComponent";

/**
 * A wrapper is a logical container for an audio element, whether it is
 * a node, a link, a port or a parameter. The main wrapper interface holds
 * the logic for all the generic behaviours :
 * - Identifying an element with its unique UUID.
 * - linking the wrapper to a GUI component inheriting from IGui.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default interface IWrapper {
  /**
   * The unique identifier of the Wrapper, useful to track down some bugs
   * by knowing if two elements are referenced as the same component or not.
   */
  uuid: string;
}