import INodeOptions from "./INodeOptions";

/**
 * Represents a category of tools in the toolbox.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default interface ICategoryOptions {
  
  // The name of the category, displayed in the interface.
  name: string;
  // The tools included in this category.
  tools: INodeOptions[];
}