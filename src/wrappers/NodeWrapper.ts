import { IWrapper } from "..";
import { v4 as uuid } from 'uuid';
import INodeOptions from "../interfaces/options/INodeOptions";
import NodeComponent from "../components/NodeComponent";


/**
 * A node wrapper contains an audio node as defined in the web audio API, for
 * exammple a GainNode for volume modifications purpose. THe wrapper can
 * dynamically build the parameters its corresponding Gui will have to display
 * by analyzing the configuration object given when building it.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default class NodeWrapper implements IWrapper {
  uuid: string;
  // The Web Audio API node wrapped by this object.
  audioNode: AudioNode;
  // The audio context, mainly used to get the context's current time.
  context: AudioContext;

  component: NodeComponent;

  constructor(context: AudioContext, options: INodeOptions) {
    this.uuid = uuid();
    this.context = context;
    this.audioNode = options.factory(this.context);
    this.component = new NodeComponent(this);
  }
}