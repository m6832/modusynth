import ICategoryOptions from "../interfaces/options/ICategoryOptions";
import createOscillator from "../factories/createOscillator";
import NumericParamWrapper from "../wrappers/NumericParamWrapper";
import ToolNames from "../utils/ToolNames";

const tools: ICategoryOptions[] = [
  {
    name: "inputs",
    tools: [
      {
        name: ToolNames.OSCILLATOR,
        icon: "mdi-wave",
        factory: createOscillator,
        parameters: [
          {
            name: "frequency",
            minimum: 0,
            maximum: 4000,
            precision: .01,
            wrapper: NumericParamWrapper
          }
        ]
      }
    ]
  }
];

export default tools;