# Modusynth

This library offers an API as simple as possible to build a modular synthetizer in a browser environment.

## What is a modular synthetizer ?

A modular synthetizer (abbreviated MS) is a specific type of synthetizer that are represented by an audio graph going from one or several sources to an output, passing through nodes that transform the audio signal. An example of node can be the gain that allow the modification of the volume of the audio signal, making it louder or quieter as you change its value.

## Agnostic ?

Yes, this library is made to be framework-agnostic. It is to say that except for the coordinates, width and height of a node, and generic 2D representation attributes like that, there won't be consideration about too much precise parameters that would tie it to a specific framework.