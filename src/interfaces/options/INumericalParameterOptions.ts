import IParameterOptions from "./IParameterOptions";

export default interface INumericalParameterOptions extends IParameterOptions {
  minimum: number;
  maximum: number;
  precision: number;
}