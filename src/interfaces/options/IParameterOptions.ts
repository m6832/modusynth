import ParamWrapper from "../../wrappers/ParamWrapper";

/**
 * This interface holds fields describing a parameter linked to a
 * node creation tool in the tools bar. It indicates which component
 * should be displaying this parameter in the UI.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default interface IParameterOptions {
  /**
   * The name of the parameter, giving the instancied ParamWrapper
   * its default name (editable by the user).
   */
  name: string;

  /**
   * Holds the class of the wrapper that will be instanciated when
   * creating the wrapper for this parameter configuration.
   */
  wrapper: typeof ParamWrapper;
}