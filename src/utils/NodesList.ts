import { NodeWrapper } from "..";

export default class NodesList {
  list: NodeWrapper[];

  constructor() {
    this.list = [];
  }

  /**
   * Determines if a node is in the list by searching for its UUID.
   * @param {NodeWrapper} node The node to check the existence in the list.
   * @return {boolean} TRUE if the node is in the list, FALSE otherwise.
   */
  contains(node: NodeWrapper): boolean {
    return this.list.find(n => n.uuid == node.uuid) !== undefined;
  }

  /**
   * Adds a node to the current list if it does not exist already.
   * @param node the node to add to the current list.
   */
  add(node: NodeWrapper) {
    if (!this.contains(node)) this.list.push(node);
  }

  indexOf(node: NodeWrapper): number {
    return this.list.map(n => n.uuid).indexOf(node.uuid);
  }

  remove(node: NodeWrapper) {
    if (this.contains(node)) this.list.splice(this.indexOf(node));
  }
}