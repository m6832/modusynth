import { v4 as uuid } from 'uuid';
import IComponent from "../interfaces/IComponent";
import { NodeWrapper } from ".."

/**
 * The graphical representation for a node. This does give accessors to
 * its coordinates (X and Y) and its dimensions (width and height).
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default class NodeComponent implements IComponent {
  uuid: string;
  wrapper: NodeWrapper;

  // The number of pixels from the left side of the window.
  x: number;
  // The number of pixels from the top of the window.
  y: number;
  // The number of pixels between the left and right sides of the node.
  width: number;
  // The number of pixels between the top and bottom of the node.
  height: number;
  
  constructor(wrapper: NodeWrapper) {
    this.wrapper = wrapper;
    this.uuid = uuid();
    this.x = 20;
    this.y = 20;
    this.width = 200;
    this.height = 100;
  }
}