import IWrapper from "../interfaces/IWrapper";
import { v4 as uuid } from "uuid";
import IParameterOptions from "../interfaces/options/IParameterOptions";
import { NodeWrapper } from "..";

/**
 * Holds the logic about the web audio API parameter it wraps, and the 
 */
export default class ParamWrapper implements IWrapper {
  uuid: string;
  // The node containing this parameter.
  node: NodeWrapper;
  // The name of the parameter. This COULD be made editable, but it is not mandatory.
  name: string;

  constructor(node: NodeWrapper, options: IParameterOptions) {
    this.uuid = uuid();
    this.node = node;
    this.name = options.name;
  }
}